#!/bin/bash
image_file=$1
repo=$2
output_dir=dependencies/


while read i; do
  docker pull $i
  trunc=$(echo $i | cut -d "/" -f 2,3)
  docker tag $i $repo/$trunc
  docker save $repo/$trunc  -o "$output_dir/$(echo $i | cut -d '/' -f 3 |cut -d ":" -f 1).tar"
done <$image_file
