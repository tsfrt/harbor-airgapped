#!/bin/bash

regA=()
while IFS= read -r line; do
   regA+=("$line")
done <<<"$(grep "registry:" $1 | cut -d ":" -f 2)"

repA=()
while IFS= read -r line; do
   repA+=("$line")
done <<<"$(grep "repository:" $1 | cut -d ":" -f 2)"

tagA=()
while IFS= read -r line; do
   tagA+=("$line")
done <<<"$(grep "tag:" $1 | cut -d ":" -f 2)"

rm $2
images=()
for i in "${!regA[@]}"; do
	echo $(echo "${regA[$i]}/${repA[$i]}:${tagA[$i]}" | tr -d " ") >> $2
done

