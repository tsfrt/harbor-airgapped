# Air Gapped Contour + Harbor Install

##Pre-reqs

### Local Pre-reqs

- helm

### Local Image Prep

```bash
helm show values bitnami/harbor  > harbor.yaml
helm show values bitnami/postgresql >postgresql.yaml
helm show values bitnami/redis > redis.yaml
helm show values bitnami/contour > contour.yaml

#get the CRD's for contour
curl -o contour-crd.yaml https://raw.githubusercontent.com/projectcontour/contour/v1.10.0/examples/contour/01-crds.yaml

#extract images
./extract-images.sh harbor.yaml harbor-gen
./extract-images.sh postgresql.yaml postgresql-gen
./extract-images.sh redis.yaml redis-gen
./extract-images.sh contour.yaml contour-gen

#pull images
./image-pull.sh harbor-gen <your temp registry ip> 
./image-pull.sh postgresql-gen <your temp registry ip> 
./image-pull.sh redis-gen <your temp registry ip> 
./image-pull.sh contour-gen <your temp registry ip> 

#with helm 3 you may see the following error
#manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
#This process has you apply the CRDs before the chart from the file we downloaded above
helm template contour bitnami/contour -n contour \
--set global.imageRegistry=10.0.51.83 \
--set envoy.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-internal"="true" \
--set envoy.service.annotations."service\.beta\.kubernetes\.io/aws-load-balancer-type"="nlb" > contour-install.yaml

helm template harbor bitnami/harbor -n harbor \
--set global.imageRegistry=10.0.51.83  \
--set externalURL=harbor.tools.tkg-demo.info \
--set ingress.enabled=true \
--set service.tls.existingSecret=harbor-cert \
--set ingress.hosts.core=harbor.tools.tkg-demo.info \
--set ingress.hosts.notary=notary.tools.tkg-demo.info > harbor-install.yaml
```

## Local cert prep
You are going to need a key pair from a trusted CA to deploy Harbor and your ingress controller.

Also, ensure that your cert is named based a domain and DNS solution you will be able to use.

In an air gapped environment you will likely be provided with signed CA certs,  ideally try to get a wild card cert that supports all your services.

Upload your key pair and any supporting certs.

## Upload to Air Gapped

#from inside project dir
tar czf /tmp/harbor.tar . 

aws s3 cp /tmp/harbor.tar s3://<your bucket>

aws s3api put-object-acl --bucket <name of existing/created bucket, name from above> --key harbor.tar --acl public-read

## Log into your Air Gapped Environment
Use your approved mechanism for accessing your environment, if you used the TKG install here, use session manager to get on your air gapped jump box.

The following instrucations assume you are picking up from the air gapped TKG install directions

```bash
sudo su - ubuntu

mkdir harbor
cd harbor


curl -o harbor.tar https://<bucket name>.s3.< your region, e.g. us-east-2 >.amazonaws.com/harbor.tar -vL

tar xvf harbor.tar 

#load images
chmod +x image-push.sh

./image-push.sh contour-gen 10.0.51.83 dependencies/
./image-push.sh harbor-gen 10.0.51.83 dependencies/
./image-push.sh redis-gen 10.0.51.83 dependencies/
./image-push.sh postgresql-gen 10.0.51.83 dependencies/

```
## Create a services cluster
It probably makes sense to create a dedicated cluster for internal services, like harbor

Some things to note for air gapped TKG:

- We will use  the air gapped TKG install registry one more time to get Harbor running.
- Set the AIRGAPPED_B64ENCODED_CA value to your new CA cert.  We will want our clusters to trust this CA going forward.

```bash
#to get the ca cert value
cat <ca cert file> | base64 -w0

```
- After setting the CA value, Source your environment variables. Also, do not forget to setup containerd config before creating the new cluster.

```bash
#this will allow the new cluster to use the install registry
export AIRGAPPED_B64ENCODED_TOML=$(envsubst < templates/containerd-config.toml | base64 -w0)

```

- Follow the instructions [here](https://repo1.dsop.io/platform-one/distros/vmware/tkg/-/blob/master/docs/mgmt.md) for creating a cluster.

```bash

#as an example, this would get you started
tkg create cluster services-cluster-01 --plan prod-airgap -w3

```

Once your cluster has created, be sure to switch contexts

```bash
tkg get credentials < your new cluster>

#see output of get credentials
kubectl config use-context <your new clusters>

```
Make sure you have a default storage class at this point.

```yaml

apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: aws-sc
  annotations:
    storageclass.beta.kubernetes.io/is-default-class: "true"
provisioner: kubernetes.io/aws-ebs

```

### Create names spaces and TLS Secret

```bash

kubectl create ns harbor
kubectl create ns contour

#this needs to match the name you specified in your template
#the name of this cert should match
#--set service.tls.existingSecret=harbor-cert 
#that you set when create your harbor template and ingress config
#note: there are a bunch of ways to configure TLS for a route
kubectl create secret tls contour-cert --cert=path/to/cert/file --key=path/to/key/file -n harbor

```

## Deploy Ingress

```bash
kubectl apply -f contour-install.yaml -n contour

#check the status
#make sure your load balancer gets an external ip
kubectl get all -n contour
```

Once the load balancer gets assigned an external ip, use that to create a record for your wildcard cert

## Deploy Harbor

```bash

kubectl apply -f harbor-install.yaml -n harbor

#verify your install
#harbor can take some time to reconcile
kubectl get all -n harbor


## Prepare Harbor

## Deploy Tools Cluster

## Deploy Contour

## Deploy Harbor


