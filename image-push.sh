#!/bin/bash
image_file=$1
repo=$2
output_dir=dependencies/

while read i; do
  tagged_image="$output_dir/$(echo $i | cut -d '/' -f 3 |cut -d ":" -f 1).tar"
  docker load -i $tagged_image
  trunc=$(echo $i | cut -d "/" -f 2,3)
  docker push $repo/$trunc
done <$image_file